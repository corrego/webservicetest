﻿namespace WebServiceTester
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkNoCert = new System.Windows.Forms.CheckBox();
            this.txtEstadoCarga = new System.Windows.Forms.TextBox();
            this.textPassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textUser = new System.Windows.Forms.TextBox();
            this.chkAutenticar = new System.Windows.Forms.CheckBox();
            this.pxLoading = new System.Windows.Forms.PictureBox();
            this.btnCargar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textUrl = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lstOperaciones = new System.Windows.Forms.ListBox();
            this.cmbServicios = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pxEjecutando = new System.Windows.Forms.PictureBox();
            this.btnEjecutar = new System.Windows.Forms.Button();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pgParametros = new System.Windows.Forms.PropertyGrid();
            this.tvParametros = new System.Windows.Forms.TreeView();
            this.txtEstadoEjecucion = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pgResultado = new System.Windows.Forms.PropertyGrid();
            this.tvResultado = new System.Windows.Forms.TreeView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pxLoading)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pxEjecutando)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Field");
            this.imageList1.Images.SetKeyName(1, "Property");
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkNoCert);
            this.groupBox1.Controls.Add(this.txtEstadoCarga);
            this.groupBox1.Controls.Add(this.textPassword);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textUser);
            this.groupBox1.Controls.Add(this.chkAutenticar);
            this.groupBox1.Controls.Add(this.pxLoading);
            this.groupBox1.Controls.Add(this.btnCargar);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textUrl);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(528, 234);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Web Service Location";
            // 
            // chkNoCert
            // 
            this.chkNoCert.AutoSize = true;
            this.chkNoCert.Location = new System.Drawing.Point(9, 118);
            this.chkNoCert.Name = "chkNoCert";
            this.chkNoCert.Size = new System.Drawing.Size(192, 17);
            this.chkNoCert.TabIndex = 11;
            this.chkNoCert.Text = "Discard certificate info (SSL only)";
            this.chkNoCert.UseVisualStyleBackColor = true;
            this.chkNoCert.CheckedChanged += new System.EventHandler(this.chkNoCert_CheckedChanged);
            // 
            // txtEstadoCarga
            // 
            this.txtEstadoCarga.Location = new System.Drawing.Point(28, 142);
            this.txtEstadoCarga.Multiline = true;
            this.txtEstadoCarga.Name = "txtEstadoCarga";
            this.txtEstadoCarga.ReadOnly = true;
            this.txtEstadoCarga.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtEstadoCarga.Size = new System.Drawing.Size(413, 86);
            this.txtEstadoCarga.TabIndex = 10;
            // 
            // textPassword
            // 
            this.textPassword.Enabled = false;
            this.textPassword.Location = new System.Drawing.Point(285, 85);
            this.textPassword.Name = "textPassword";
            this.textPassword.Size = new System.Drawing.Size(156, 22);
            this.textPassword.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(210, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Password:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "User:";
            // 
            // textUser
            // 
            this.textUser.Enabled = false;
            this.textUser.Location = new System.Drawing.Point(62, 85);
            this.textUser.Name = "textUser";
            this.textUser.Size = new System.Drawing.Size(131, 22);
            this.textUser.TabIndex = 6;
            // 
            // chkAutenticar
            // 
            this.chkAutenticar.AutoSize = true;
            this.chkAutenticar.Location = new System.Drawing.Point(9, 62);
            this.chkAutenticar.Name = "chkAutenticar";
            this.chkAutenticar.Size = new System.Drawing.Size(92, 17);
            this.chkAutenticar.TabIndex = 5;
            this.chkAutenticar.Text = "Authenticate";
            this.chkAutenticar.UseVisualStyleBackColor = true;
            this.chkAutenticar.CheckedChanged += new System.EventHandler(this.chkAutenticar_CheckedChanged);
            // 
            // pxLoading
            // 
            this.pxLoading.Location = new System.Drawing.Point(6, 177);
            this.pxLoading.Name = "pxLoading";
            this.pxLoading.Size = new System.Drawing.Size(16, 16);
            this.pxLoading.TabIndex = 3;
            this.pxLoading.TabStop = false;
            // 
            // btnCargar
            // 
            this.btnCargar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCargar.Enabled = false;
            this.btnCargar.Location = new System.Drawing.Point(447, 34);
            this.btnCargar.Name = "btnCargar";
            this.btnCargar.Size = new System.Drawing.Size(75, 23);
            this.btnCargar.TabIndex = 2;
            this.btnCargar.Text = "Load";
            this.btnCargar.UseVisualStyleBackColor = true;
            this.btnCargar.Click += new System.EventHandler(this.btnCargar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Service Url:";
            // 
            // textUrl
            // 
            this.textUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textUrl.Location = new System.Drawing.Point(6, 34);
            this.textUrl.Name = "textUrl";
            this.textUrl.Size = new System.Drawing.Size(435, 22);
            this.textUrl.TabIndex = 0;
            this.textUrl.TextChanged += new System.EventHandler(this.textUrl_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lstOperaciones);
            this.groupBox2.Controls.Add(this.cmbServicios);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 252);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(528, 196);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Service Information";
            // 
            // lstOperaciones
            // 
            this.lstOperaciones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstOperaciones.FormattingEnabled = true;
            this.lstOperaciones.Location = new System.Drawing.Point(155, 61);
            this.lstOperaciones.Name = "lstOperaciones";
            this.lstOperaciones.Size = new System.Drawing.Size(367, 121);
            this.lstOperaciones.TabIndex = 3;
            this.lstOperaciones.SelectedIndexChanged += new System.EventHandler(this.lstOperaciones_SelectedIndexChanged);
            // 
            // cmbServicios
            // 
            this.cmbServicios.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbServicios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbServicios.FormattingEnabled = true;
            this.cmbServicios.Location = new System.Drawing.Point(155, 24);
            this.cmbServicios.Name = "cmbServicios";
            this.cmbServicios.Size = new System.Drawing.Size(367, 21);
            this.cmbServicios.TabIndex = 2;
            this.cmbServicios.SelectedIndexChanged += new System.EventHandler(this.cmbServicios_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Supported operations:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Available Endpoints:";
            // 
            // pxEjecutando
            // 
            this.pxEjecutando.Location = new System.Drawing.Point(152, 177);
            this.pxEjecutando.Name = "pxEjecutando";
            this.pxEjecutando.Size = new System.Drawing.Size(16, 16);
            this.pxEjecutando.TabIndex = 5;
            this.pxEjecutando.TabStop = false;
            // 
            // btnEjecutar
            // 
            this.btnEjecutar.Enabled = false;
            this.btnEjecutar.Location = new System.Drawing.Point(20, 172);
            this.btnEjecutar.Name = "btnEjecutar";
            this.btnEjecutar.Size = new System.Drawing.Size(126, 27);
            this.btnEjecutar.TabIndex = 4;
            this.btnEjecutar.Text = "Invoke";
            this.btnEjecutar.UseVisualStyleBackColor = true;
            this.btnEjecutar.Click += new System.EventHandler(this.btnEjecutar_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCerrar.Location = new System.Drawing.Point(1055, 474);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(75, 23);
            this.btnCerrar.TabIndex = 4;
            this.btnCerrar.Text = "Close";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.pgParametros);
            this.groupBox4.Controls.Add(this.tvParametros);
            this.groupBox4.Controls.Add(this.txtEstadoEjecucion);
            this.groupBox4.Controls.Add(this.btnEjecutar);
            this.groupBox4.Controls.Add(this.pxEjecutando);
            this.groupBox4.Location = new System.Drawing.Point(546, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(584, 234);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Operation Invocation";
            // 
            // pgParametros
            // 
            this.pgParametros.HelpVisible = false;
            this.pgParametros.Location = new System.Drawing.Point(266, 18);
            this.pgParametros.Name = "pgParametros";
            this.pgParametros.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.pgParametros.Size = new System.Drawing.Size(312, 118);
            this.pgParametros.TabIndex = 13;
            this.pgParametros.ToolbarVisible = false;
            // 
            // tvParametros
            // 
            this.tvParametros.Location = new System.Drawing.Point(6, 18);
            this.tvParametros.Name = "tvParametros";
            this.tvParametros.Size = new System.Drawing.Size(254, 118);
            this.tvParametros.TabIndex = 12;
            this.tvParametros.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvParametros_AfterSelect);
            // 
            // txtEstadoEjecucion
            // 
            this.txtEstadoEjecucion.Location = new System.Drawing.Point(174, 142);
            this.txtEstadoEjecucion.Multiline = true;
            this.txtEstadoEjecucion.Name = "txtEstadoEjecucion";
            this.txtEstadoEjecucion.ReadOnly = true;
            this.txtEstadoEjecucion.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtEstadoEjecucion.Size = new System.Drawing.Size(404, 86);
            this.txtEstadoEjecucion.TabIndex = 11;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pgResultado);
            this.groupBox3.Controls.Add(this.tvResultado);
            this.groupBox3.Location = new System.Drawing.Point(546, 252);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(584, 196);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Response";
            // 
            // pgResultado
            // 
            this.pgResultado.Enabled = false;
            this.pgResultado.HelpVisible = false;
            this.pgResultado.Location = new System.Drawing.Point(266, 24);
            this.pgResultado.Name = "pgResultado";
            this.pgResultado.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.pgResultado.Size = new System.Drawing.Size(312, 138);
            this.pgResultado.TabIndex = 6;
            this.pgResultado.ToolbarVisible = false;
            // 
            // tvResultado
            // 
            this.tvResultado.Location = new System.Drawing.Point(6, 24);
            this.tvResultado.Name = "tvResultado";
            this.tvResultado.Size = new System.Drawing.Size(254, 138);
            this.tvResultado.TabIndex = 5;
            this.tvResultado.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvResultado_AfterSelect);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1142, 506);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Web Service Test Application";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pxLoading)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pxEjecutando)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCargar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textUrl;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pxLoading;
        private System.Windows.Forms.ListBox lstOperaciones;
        private System.Windows.Forms.ComboBox cmbServicios;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnEjecutar;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.PictureBox pxEjecutando;
        private System.Windows.Forms.TextBox textPassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textUser;
        private System.Windows.Forms.CheckBox chkAutenticar;
        private System.Windows.Forms.TextBox txtEstadoCarga;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PropertyGrid pgParametros;
        private System.Windows.Forms.TreeView tvParametros;
        private System.Windows.Forms.TextBox txtEstadoEjecucion;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PropertyGrid pgResultado;
        private System.Windows.Forms.TreeView tvResultado;
        private System.Windows.Forms.CheckBox chkNoCert;
    }
}

