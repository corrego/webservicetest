﻿/**
 * WebServiceTest
 *
 * Released under the MIT License
 *
 * Copyright (c) 2012 Carlos Orrego
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

using System;
using System.ComponentModel;
using System.Net;
using System.Windows.Forms;
using WebServiceTester.Properties;

namespace WebServiceTester
{
    public partial class MainForm : Form
    {
        private EndpointInfo[] endpoints;

        public MainForm()
        {
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void textUrl_TextChanged(object sender, EventArgs e)
        {
            btnCargar.Enabled = !string.IsNullOrEmpty(textUrl.Text);
        }

        private void btnCargar_Click(object sender, EventArgs e)
        {
            DisableAll();
            ClearAll();
            pxLoading.Image = Resources.Loading;

            var bw = new BackgroundWorker();
            bool useAuth = chkAutenticar.Checked;
            string userName = textUser.Text;
            string password = textPassword.Text;
            string url = textUrl.Text;

            bw.DoWork += (a, b) =>
                         {
                             // get endpoints for URL
                             endpoints = useAuth
                                             ? EndpointInfo.GetEndpointsForUrl(new Uri(url), userName, password)
                                             : EndpointInfo.GetEndpointsForUrl(new Uri(url));
                         };

            bw.RunWorkerCompleted += (a, result) =>
                                     {
                                         EnableAll();
                                         if (result.Error == null)
                                         {
                                             pxLoading.Image = Resources.Ok;
                                             txtEstadoCarga.Text = "Operation Completed.";

                                             // add to combo
                                             cmbServicios.Items.Clear();
                                             foreach (EndpointInfo service in endpoints)
                                             {
                                                 cmbServicios.Items.Add(service);
                                             }
                                             if (cmbServicios.Items.Count != 0)
                                             {
                                                 cmbServicios.SelectedIndex = 0;
                                             }
                                         }
                                         else
                                         {
                                             pxLoading.Image = Resources.Error;
                                             txtEstadoCarga.Text = result.Error.Message;
                                             if (result.Error.InnerException != null)
                                             {
                                                 txtEstadoCarga.Text += Environment.NewLine +
                                                                        result.Error.InnerException.Message;
                                             }
                                         }
                                     };
            bw.RunWorkerAsync();
        }

        private void DisableAll()
        {
            textUrl.Enabled = false;
            btnCargar.Enabled = false;
            cmbServicios.Enabled = false;
            lstOperaciones.Enabled = false;
            pgParametros.Enabled = false;
            pgResultado.Enabled = false;
            tvParametros.Enabled = false;
            tvResultado.Enabled = false;
            btnEjecutar.Enabled = false;
        }

        private void ClearAll()
        {
            cmbServicios.Items.Clear();
            lstOperaciones.Items.Clear();
            tvResultado.Nodes.Clear();
            tvParametros.Nodes.Clear();
            pgResultado.SelectedObject = null;
            pgParametros.SelectedObject = null;
            txtEstadoCarga.Clear();
            txtEstadoEjecucion.Clear();
            pxLoading.Image = null;
            pxEjecutando.Image = null;
        }

        private void EnableAll()
        {
            textUrl.Enabled = true;
            btnCargar.Enabled = true;
            cmbServicios.Enabled = true;
            lstOperaciones.Enabled = true;
            pgParametros.Enabled = true;
            tvParametros.Enabled = true;
            tvResultado.Enabled = true;
            btnEjecutar.Enabled = true;
        }

        private void cmbServicios_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstOperaciones.Items.Clear();
            if (cmbServicios.SelectedIndex != -1)
            {
                lstOperaciones.Items.AddRange(endpoints[cmbServicios.SelectedIndex].Methods);

                if (lstOperaciones.Items.Count > 0)
                {
                    lstOperaciones.SelectedIndex = 0;
                    btnEjecutar.Enabled = true;
                }
                else
                    btnEjecutar.Enabled = false;
            }
        }

        private void lstOperaciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            // show parameters
            if (lstOperaciones.SelectedIndex != -1)
            {
                ServiceMethodInfo method = endpoints[cmbServicios.SelectedIndex].Methods[lstOperaciones.SelectedIndex];

                tvParametros.Nodes.Clear();
                tvParametros.Nodes.Add(method.ParametersTree);
            }
        }

        private void dgParametros_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
        }

        private void btnEjecutar_Click(object sender, EventArgs e)
        {
            pxEjecutando.Image = Resources.Loading;

            var bw = new BackgroundWorker();
            DisableAll();

            int service = cmbServicios.SelectedIndex;
            int method = lstOperaciones.SelectedIndex;

            bw.DoWork += (a, b) => b.Result = endpoints[service].Invoke(endpoints[service].Methods[method]);


            bw.RunWorkerCompleted += (a, result) =>
                                     {
                                         EnableAll();
                                         if (result.Error == null)
                                         {
                                             pxEjecutando.Image = Resources.Ok;
                                             txtEstadoEjecucion.Text = "Operation Completed.";

                                             // show result
                                             tvResultado.Nodes.Clear();
                                             tvResultado.Nodes.Add(ServiceMethodInfo.GetResultTree(result.Result));
                                         }
                                         else
                                         {
                                             pxEjecutando.Image = Resources.Error;
                                             txtEstadoEjecucion.Text = result.Error.Message;
                                             if (result.Error.InnerException != null)
                                             {
                                                 txtEstadoEjecucion.Text += Environment.NewLine +
                                                                            result.Error.InnerException.Message;
                                             }
                                         }
                                     };
            bw.RunWorkerAsync();
        }

        private void chkAutenticar_CheckedChanged(object sender, EventArgs e)
        {
            textUser.Enabled = textPassword.Enabled = chkAutenticar.Checked;
        }

        private void tvParametros_AfterSelect(object sender, TreeViewEventArgs e)
        {
            pgParametros.SelectedObject = e.Node.Tag;
        }

        private void tvResultado_AfterSelect(object sender, TreeViewEventArgs e)
        {
            pgResultado.SelectedObject = e.Node.Tag;
        }

        private void chkNoCert_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNoCert.Checked)
            {
                ServicePointManager.ServerCertificateValidationCallback =
                    ((ssender, certificate, chain, sslPolicyErrors) => true);
            }
            else
            {
                ServicePointManager.ServerCertificateValidationCallback = null;
            }
        }
    }
}