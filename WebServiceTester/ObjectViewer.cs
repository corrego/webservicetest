﻿/**
 * WebServiceTest
 *
 * Released under the MIT License
 *
 * Copyright (c) 2012 Carlos Orrego
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

using System;
using System.Collections;
using System.Reflection;
using System.Windows.Forms;

namespace WebServiceTester
{
    internal class ObjectViewer
    {
        private readonly Hashtable reflectedObjects = new Hashtable();

        public TreeNode GetRootForObject(object o)
        {
            var node = new TreeNode("");

            reflectedObjects.Clear();

            DrawMember(o, node);
            node.Text = node.Text.Insert(0, o.GetType().ToString());
            node.ImageKey = "Field";
            node.SelectedImageKey = "Field";
            return node;
        }


        private void DrawMember(object o, TreeNode node)
        {
            if (o == null)
                return;

            Type type = o.GetType();

            if (!type.Equals(typeof (string)) && !type.IsValueType)
            {
                if (reflectedObjects.Contains(o))
                {
                    node.Text += " : " + (string) reflectedObjects[o];
                    return;
                }

                reflectedObjects.Add(o, "{" + node.Text + " : " + o + "}");

                MemberInfo[] members = type.GetMembers(BindingFlags.Public | BindingFlags.Instance);

                for (int i = 0; members != null && i < members.Length; i++)
                {
                    MemberInfo member = members[i];

                    TreeNode child;
                    object oChild;

                    switch (member.MemberType)
                    {
                        case MemberTypes.Field:
                            child = new TreeNode(member.Name);
                            child.ImageKey = "Field";
                            child.SelectedImageKey = "Field";
                            node.Nodes.Add(child);

                            oChild = ((FieldInfo) member).GetValue(o);

                            ParseCollectionMember(oChild, child);

                            if (oChild != null)
                                DrawMember(oChild, child);
                            else
                                child.Text += " : (null)";

                            break;
                        case MemberTypes.Property:

                            var property = (PropertyInfo) member;
                            if (!property.CanRead)
                                break;

                            child = new TreeNode(member.Name);
                            child.ImageKey = "Property";
                            child.SelectedImageKey = "Property";
                            node.Nodes.Add(child);


                            System.Reflection.ParameterInfo[] parameters = property.GetIndexParameters();

                            if (parameters.Length == 0)
                            {
                                oChild = property.GetValue(o, null);

                                ParseCollectionMember(oChild, child);

                                if (oChild != null)
                                    DrawMember(oChild, child);
                                else
                                    child.Text += " : (null)";
                            }
                            else
                                child.Text += " : <Cannot view indexed property>";

                            break;
                    }
                }
            }
            else if (o is DateTime)
                node.Text += " : " + ((DateTime) o);
            else
                node.Text += " : " + o;
        }

        private void ParseCollectionMember(object oChild, TreeNode child)
        {
            IEnumerable enumerable;
            int j;

            if (oChild is IEnumerable && !(oChild is string))
            {
                var collectionchild = new TreeNode("{" + oChild.GetType().FullName + "}");
                child.Nodes.Add(collectionchild);

                enumerable = (IEnumerable) oChild;
                j = 0;

                foreach (object obj in enumerable)
                {
                    var subchild = new TreeNode("[" + j + "]");
                    collectionchild.Nodes.Add(subchild);

                    DrawMember(obj, subchild);
                    j++;
                }
            }
        }
    }
}