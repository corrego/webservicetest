﻿/**
 * This file was originally part of WebServiceStudio (http://webservicestudio.codeplex.com/).
 * It is probably different than the original version.
 **/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace WebServiceTester.ObjectEditor
{
    internal class MethodProperty : TreeNodeProperty
    {
        private readonly bool isIn;
        private readonly MethodInfo method;
        private object[] paramValues;
        private object result;

        public MethodProperty(MethodInfo method)
            : base(new[] {method.ReturnType}, method.Name)
        {
            this.method = method;
            isIn = true;
        }

        public static TreeNode GetNodeForResult(object value)
        {
            if (value == null)
            {
                return null;
            }

            var parentNode = new TreeNode();
            Type type = value.GetType();
            CreateTreeNodeProperty(new[] {type}, "result", value).RecreateSubtree(parentNode);
            return parentNode.Nodes[0];
        }

        public TreeNode GetNodeForMethod()
        {
            var parentNode = new TreeNode(method.Name);
            System.Reflection.ParameterInfo[] parameters = method.GetParameters();
            for (int i = 0; i < parameters.Length; i++)
            {
                if ((!isIn && (parameters[i].IsOut || parameters[i].ParameterType.IsByRef)) || (isIn && !parameters[i].IsOut))
                {
                    Type parameterType = parameters[i].ParameterType;
                    if (parameterType.IsByRef)
                    {
                        parameterType = parameterType.GetElementType();
                    }
                    object val = (paramValues != null) ? paramValues[i] : (isIn ? CreateNewInstance(parameterType) : null);
                    CreateTreeNodeProperty(base.GetIncludedTypes(parameterType), parameters[i].Name, val).RecreateSubtree(parentNode);
                }
            }
            parentNode.ExpandAll();
            TreeNode = parentNode;
            return parentNode;
        }


        //protected override MethodInfo GetCurrentMethod()
        //{
        //    return this.method;
        //}

        //protected override object GetCurrentProxy()
        //{
        //    return this.proxyProperty.GetProxy();
        //}

        //public MethodInfo GetMethod()
        //{
        //    return this.method;
        //}

        //public ProxyProperty GetProxyProperty()
        //{
        //    return this.proxyProperty;
        //}

        //protected override bool IsInput()
        //{
        //    return this.isIn;
        //}

        private void ReadBody()
        {
            TreeNode node = base.TreeNode; //.Nodes[0];
            System.Reflection.ParameterInfo[] parameters = method.GetParameters();
            paramValues = new object[parameters.Length];
            int index = 0;
            int num2 = 0;
            while (index < paramValues.Length)
            {
                System.Reflection.ParameterInfo info = parameters[index];
                if (!info.IsOut)
                {
                    TreeNode node2 = node.Nodes[num2++];
                    var tag = node2.Tag as TreeNodeProperty;
                    if (tag != null)
                    {
                        paramValues[index] = tag.ReadChildren();
                    }
                }
                index++;
            }
        }

        public override object ReadChildren()
        {
            //this.ReadHeaders();
            ReadBody();
            return paramValues;
        }

        public override string ToString()
        {
            return base.Name;
        }

        public static Type GetGenericIEnumerables(object o)
        {
            return o.GetType()
                .GetInterfaces()
                .Where(t => t.IsGenericType
                            && t.GetGenericTypeDefinition() == typeof (IEnumerable<>))
                .Select(t => t.GetGenericArguments()[0]).FirstOrDefault();
        }
    }
}