﻿/**
 * This file was originally part of WebServiceStudio (http://webservicestudio.codeplex.com/).
 * It is probably different than the original version.
 **/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace WebServiceTester.ObjectEditor
{
    class IListProperty:ClassProperty
    {
        public IListProperty(Type[] possibleTypes, string name, IList val) : base(possibleTypes, name, val)
        {
            underlyingType = GetGenericIEnumerable(val);
        }

        private Type underlyingType;

        private IList IListValue
        {
            get { return (base.InternalValue as IList); }
            set { base.InternalValue = value; }
        }


        private static Type GetGenericIEnumerable(object o)
        {
            return o.GetType()
                .GetInterfaces()
                .Where(t => t.IsGenericType
                            && t.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                .Select(t => t.GetGenericArguments()[0]).FirstOrDefault();
        }

        [RefreshProperties(RefreshProperties.All)]
        public virtual int Length
        {
            get { return ((IListValue != null) ? IListValue.Count : 0); }
            set
            {
                int length = Length;
                int num2 = value;

                if(num2-length>=0)
                {
                    // agregar nuevos elementos
                    for (int i = 0; i < num2-length; i++)
                    {
                        IListValue.Add(CreateNewInstance(underlyingType));
                    }
                }
                else
                {
                    // eliminar elementos sobrantes
                    for (int i = 0; i < length-num2; i++)
                    {
                        IListValue.RemoveAt(num2);
                    }
                }
                //Array destinationArray = Array.CreateInstance(Type.GetElementType(), num2);
                //if (ArrayValue != null)
                //{
                //    Array.Copy(ArrayValue, destinationArray, Math.Min(num2, length));
                //}
                //ArrayValue = destinationArray;
                base.TreeNode.Text = ToString();
                CreateChildren();
            }
        }

        protected override void CreateChildren()
        {
            base.TreeNode.Nodes.Clear();
            if (OkayToCreateChildren())
            {
                Type elementType = Type.GetElementType();
                int length = Length;
                for (int i = 0; i < length; i++)
                {
                    object val = IListValue[i];
                    if ((val == null) && IsInput())
                    {
                        val = CreateNewInstance(underlyingType);
                    }
                    CreateTreeNodeProperty(base.GetIncludedTypes(underlyingType), base.Name + "[" + i + "]", val).RecreateSubtree(base.TreeNode);
                }
            }
        }

        public override object ReadChildren()
        {
            IList listValue = IListValue;
            if (listValue == null)
            {
                return null;
            }
            int num = 0;
            for (int i = 0; i < listValue.Count; i++)
            {
                TreeNode node = base.TreeNode.Nodes[num++];
                var tag = node.Tag as TreeNodeProperty;
                if (tag != null)
                {
                    listValue[i]=tag.ReadChildren();
                }
            }
            return listValue;
        }

        public static bool IsList(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            return type.GetInterfaces().Contains(typeof (IList));
        }

    }
}
