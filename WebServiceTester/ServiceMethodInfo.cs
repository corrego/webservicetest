﻿/**
 * WebServiceTest
 *
 * Released under the MIT License
 *
 * Copyright (c) 2012 Carlos Orrego
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

using System.Reflection;
using System.Windows.Forms;
using WebServiceTester.ObjectEditor;

namespace WebServiceTester
{
    internal class ServiceMethodInfo
    {
        private readonly MethodProperty methodProperty;

        public ServiceMethodInfo(MethodInfo methodInfo)
        {
            Name = methodInfo.Name;
            methodProperty = new MethodProperty(methodInfo);
            ParametersTree = methodProperty.GetNodeForMethod();
        }

        public string Name { get; set; }
        public TreeNode ParametersTree { get; private set; }

        public static TreeNode GetResultTree(object value)
        {
            return MethodProperty.GetNodeForResult(value);
        }

        public object[] GetParamsValues()
        {
            return (object[]) methodProperty.ReadChildren();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}