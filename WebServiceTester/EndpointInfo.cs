﻿/**
 * WebServiceTest
 *
 * Released under the MIT License
 *
 * Copyright (c) 2012 Carlos Orrego
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */

using System;
using System.Collections.Generic;
using System.Reflection;
using System.ServiceModel.Description;
using WcfSamples.DynamicProxy;

namespace WebServiceTester
{
    internal class EndpointInfo
    {
        private DynamicProxy proxy;
        public string Name { get; set; }
        public ServiceMethodInfo[] Methods { get; set; }

        public static EndpointInfo[] GetEndpointsForUrl(Uri serviceUrl)
        {
            return GetEndpointsForUrl(serviceUrl, null, null);
        }

        public static EndpointInfo[] GetEndpointsForUrl(Uri serviceUrl, string userName, string password)
        {
            var infos = new List<EndpointInfo>();
            DynamicProxyFactory factory;

            if(!string.IsNullOrEmpty(userName))
            {
                factory = new DynamicProxyFactory(serviceUrl.ToString(), userName, password);
            }
            else
            {
                factory = new DynamicProxyFactory(serviceUrl.ToString());
            }

            foreach (ServiceEndpoint endpoint in factory.Endpoints)
            {
                // get endpoint info
                var endpointInfo = new EndpointInfo();
                endpointInfo.Name = endpoint.Name;
                endpointInfo.proxy = factory.CreateProxy(endpoint);

                // get method info
                MethodInfo[] methods =
                    endpointInfo.proxy.ProxyType.GetMethods(BindingFlags.Instance | BindingFlags.Public |
                                                            BindingFlags.DeclaredOnly);
                var methodInfo = new List<ServiceMethodInfo>();
                foreach (MethodInfo method in methods)
                {
                    var mInfo = new ServiceMethodInfo(method);
                    mInfo.Name = method.Name;

                    methodInfo.Add(mInfo);
                }

                endpointInfo.Methods = methodInfo.ToArray();
                infos.Add(endpointInfo);
            }

            return infos.ToArray();
        }

        public object Invoke(ServiceMethodInfo method)
        {
            object retval = proxy.ObjectType.GetMethod(method.Name).Invoke(proxy.ObjectInstance, method.GetParamsValues());

            return retval;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}